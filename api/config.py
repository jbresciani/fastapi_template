"""Configure logging."""
import os

environment = os.environ.get('ENVIRONMENT', 'dev')
