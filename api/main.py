"""Fast API endpoint configuration."""
import logging
import os

import uvicorn
from fastapi import FastAPI
from fastapi.openapi.docs import get_redoc_html, get_swagger_ui_html
from fastapi.responses import RedirectResponse
from fastapi.staticfiles import StaticFiles
from starlette_exporter import PrometheusMiddleware, handle_metrics

logger = logging.getLogger(__name__)

app = FastAPI(docs_url=None, redoc_url=None)
app.mount("/static", StaticFiles(directory="api/static"), name="static")

app.add_middleware(PrometheusMiddleware)
app.add_route("/metrics", handle_metrics)


logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.StreamHandler()
    ]
)


@app.get("/")
def read_root():
    """Root web page."""
    return "Hello World!"


@app.get('/favicon.ico', include_in_schema=False)
def favicon():
    """Set the favicon.ico file."""
    return RedirectResponse(url='/static/favicon.ico')


@app.get("/docs", include_in_schema=False)
def overridden_swagger():
    """Override favicon icon for swagger endpoint."""
    return get_swagger_ui_html(
        openapi_url="/openapi.json",
        title="FastAPI",
        swagger_favicon_url="/static/favicon.ico"
    )


@app.get("/redoc", include_in_schema=False)
def overridden_redoc():
    """Override favicon icon for redoc endpoint."""
    return get_redoc_html(
        openapi_url="/openapi.json",
        title="FastAPI",
        redoc_favicon_url="/static/favicon.ico"
    )


if __name__ == "__main__":
    uvicorn.run(app)
