# Fast api template

This is a basic fastapi template, it includes some basic, commonly used packages, poetry for python lib management and skaffold for deployment

## Requirements

* helm
* skaffold
* IDE (currently dev-in-a-container is configured for vscode)

## Starting up the Server

from inside the container run 

```bash
python -m gunicorn -k uvicorn.workers.UvicornWorker api.main:app
```

for hot-reload mode use

```bash
uvicorn api.main:app --port 8080 --host "0.0.0.0" --reload
```

## Build and launch in docker

```bash
skaffold dev
```

skaffold will stay connected and rebuild/deploy when files change (on save)

## Configured urls

localhost:8080/ # hello world page
localhost:8080/docs # swagger documentation
localhost:8080/metrics # basic prometheus metrics

# TODO

* add cloudcode to vscode config for IDE base deploy and debug into kubernetes
