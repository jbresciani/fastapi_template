ARG BUILD="unknown"
ARG BUILD_URL="local"
ARG GIT_HASH="unset"
ARG PORT=8080
# PYTHON_VERSION already exists and contains the bug level of the version number which we don't want here
ARG PYTHON_MINOR_VERSION="3.10"

FROM python:${PYTHON_MINOR_VERSION}-slim-buster as base

ARG PORT

ENV DEBIAN_FRONTEND="noninteractive" \
    DEBUG_OPTS="" \
    LANG="C.UTF-8" \    
    PIP_NO_CACHE_DIR=1 \
    PIP_DEFAULT_TIMEOUT=100 \
    PORT=$PORT \
    PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    TZ="UTC"

FROM base as builder

ARG PYTHON_MINOR_VERSION

RUN apt-get update \
    && apt-get -y install gcc \
    && rm -rf /var/lib/apt/lists/

RUN pip install -U pipenv \
                   pip \
                   wheel

COPY Pipfile Pipfile.lock ./

# we install packages like this as it eliminates any compiler requirements from packages like psycopg2 and some ldap modules from appearing in the final container
RUN pipenv requirements | pip install --no-cache-dir -U --target /required_packages/lib/python${PYTHON_MINOR_VERSION}/site-packages/ -r /dev/stdin
# Google's libraries still uses `six` for python 2.7 library translation reasons which messes with the file paths
RUN mv /required_packages/lib/python${PYTHON_MINOR_VERSION}/site-packages/bin /required_packages || echo "no bin files to move"

FROM builder as dev_in_a_container

COPY --from=builder /required_packages /usr/local
RUN pipenv requirements --dev | pip install --no-cache-dir -U --prefix /usr/local -r /dev/stdin

RUN apt-get update && apt-get install -y --no-install-recommends git

FROM base as pre_production

ARG BUILD
ARG BUILD_URL
ARG GIT_HASH

LABEL version=${BUILD} \
      build_url=${BUILD_URL} \
      git_hash=${GIT_HASH} \
      maintainer="jacob@bresciani.ca"

ENV PORT=8080 \
    LISTEN_ON="0.0.0.0"

COPY --from=builder /required_packages /usr/local

WORKDIR /opt

RUN useradd uvicorn -d /opt/api -m --system 
RUN apt-get update && apt-get install -y curl
USER uvicorn
COPY api ./api

FROM pre_production as hot_reload

CMD uvicorn api.main:app --port 8080 --host "0.0.0.0" --reload

FROM pre_production as production

CMD python -m gunicorn -k uvicorn.workers.UvicornWorker api.main:app
