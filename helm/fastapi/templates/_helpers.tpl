{{/* vim: set filetype=mustache: */}}
{{- define "api.version" -}}
{{- if .Values.version -}}
{{ .Values.version }}
{{- else if .Values.branch -}}
{{ .Values.branch }}
{{- else -}}
{{ .Release.name }}
{{- end -}}
{{- end -}}

{{- define "api.name" -}}
{{ .Values.serviceName }}-{{ include "api.version" . }}
{{- end -}}


{{- define "consul_tags" -}}
{{- $branch := .Values.branch | default "unset" -}}
{{- if eq $branch "main" -}}
[ "main" ]
{{- else -}}
[ {{ include "api.version" . | quote }} ]
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "api.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "api.labels" -}}
helm.sh/chart: {{ include "api.chart" . }}
{{ include "api.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}
{{/*
Selector labels
*/}}

{{- define "api.selectorLabels" -}}
app: {{ .Values.serviceName }}
version: {{ include "api.version" . }}
app.kubernetes.io/name: {{ include "api.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "api.serviceAccountName" }}
{{- if .Values.serviceAccount.create -}}
  {{ default (include "api.name" .) .Values.serviceAccount.name }}
{{- else -}}
  {{ default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end -}}